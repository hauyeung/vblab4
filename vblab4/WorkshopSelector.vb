﻿Public Class WorkshopSelector
    Private workshop As String
    Private loc As String
    Private costitem As String
    Private subtotals As New List(Of Double)
    Private total As Double

    Public Sub WorkshopSelector()
        total = 0
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            workshop = lstWorkshop.SelectedItem.ToString()
            loc = lstLocation.SelectedItem.ToString()
            Dim workshopcalc As workshop = New workshop()
            Dim workshopfee As Double = workshopcalc.getregfee(workshop)
            Dim lodgingfee As Double = workshopcalc.getlodgingfees(loc)
            Dim numdays As Integer = workshopcalc.getnumdays(workshop)
            Dim subtotal As Double = workshopcalc.getsubtotal(workshopfee, lodgingfee, numdays)
            subtotals.Add(subtotal)
            costitem = workshop + " - " + loc + " - $" + subtotal.ToString()
            lstCosts.Items.Add(costitem)
        Catch
            MessageBox.Show("Please add a workshop")
        End Try

    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        For Each subtotal In subtotals
            total += subtotal
        Next
        txtTotalCost.Text = "$" + total.ToString()
        total = 0
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtTotalCost.Text = ""
        lstCosts.Items.Clear()
        lstLocation.ClearSelected()
        lstWorkshop.ClearSelected()
        total = 0
        subtotals.Clear()

    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub WorkshopSelector_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim workshoptooltip As New ToolTip
        Dim locationtooltip As New ToolTip
        Dim coststooltip As New ToolTip
        Dim addwstooltip As New ToolTip
        workshoptooltip.SetToolTip(Me.grpWorkshop, "Select a workshop")
        locationtooltip.SetToolTip(Me.grpLocation, "Pick a location from below")
        coststooltip.SetToolTip(Me.grpCosts, "Selected items will be shown here")
        addwstooltip.SetToolTip(Me.btnAdd, "Click this to add workshop")

    End Sub
End Class
