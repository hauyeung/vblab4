﻿Public Class workshop
    Function getregfee(ByVal workshop As String) As Double
        Select workshop
            Case "Handling Stress"
                Return 595
            Case "Time Management"
                Return 695
            Case "Supervision Skills"
                Return 995
            Case "Negotiation"
                Return 1295
            Case "How to Interview"
                Return 395
        End Select
    End Function

    Function getnumdays(ByVal workshop As String) As Integer
        If workshop = "Handling Stress" Or workshop = "Time Management" Or workshop = "Supervision Skills" Then
            Return 3
        ElseIf workshop = "Negotiation" Then
            Return 5
        ElseIf workshop = "How to Interview" Then
            Return 1
        End If
    End Function

    Function getlodgingfees(ByVal location As String) As Double
        If location = "Austin" Then
            Return 95
        ElseIf location = "Chicago" Then
            Return 125
        ElseIf location = "Dallas" Then
            Return 110
        ElseIf location = "Orlando" Then
            Return 100
        ElseIf location = "Phoenix" Then
            Return 92
        ElseIf location = "Raleigh" Then
            Return 90
        End If
    End Function

    Function getsubtotal(ByVal workshopfee As Double, ByVal lodgingfeeperday As Double, ByVal days As Integer) As Double
        Return workshopfee + lodgingfeeperday * days
    End Function



End Class
